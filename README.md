# 更换centos yum 的源
RHEL7的yum服务是付费的，因为没有付费，所以无法使用yum安装软件，只能替换为CentOs的yum工具

-- 查看yum
rpm -qa |grep yum

--卸载yum
rpm -qa | grep yum | xargs rpm -e --nodeps

--拷贝centos系统yum工具安装程序到linux指定目录下 

python-urlgrabber-*.el*.noarch.rpm
yum-*.el*.centos.noarch.rpm 
yum-plugin-fastestmirror-*.el*.noarch.rpm
yum-updateonboot-*.el*.noarch.rpm
yum-utils-*.el*.noarch.rpm
yum-metadata-parser-*.el*.*.rpm 
可以使用wget 命令从阿里云镜像中获取对应系统版本的文件
cetos 6  32位
http://mirrors.aliyun.com/centos/6/os/i386/Packages/
centos 6 64位
http://mirrors.aliyun.com/centos/6/os/x86_64/Packages/
centos 7 64位
http://mirrors.aliyun.com/centos/7/os/x86_64/Packages/

--安装yum包
rpm -ivh yum-* 
如果出现


error: Failed dependencies:

        python-urlgrabber >= 3.9.1-10 is needed by yum-3.2.29-73.el6.centos.noarch

使用下面

rpm -Uvh python-urlgrabber-3.9.1-11.el6.noarch.rpm

更新rpm -Uvh python-urlgrabber版本，不更新肯定安装不过去

在执行：

rpm -ivh yum-*

根据centos的版本获取对应的资源文件，以下例子是centos 7的步骤，如果是centos 6，把对应的文件名数字7改为6即可。
--在阿里云开源软件镜像站点下载资源文件，目标地址是/etc/yum.repos.d/CentOS-Base.repo（覆盖写）
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

--替换CentOS-Base.repo文件$releasever变量为相关linux版本，这里替换为“7”就可以了，具体操作：
vi /etc/yum.repos.d/CentOS-Base.repo
:%s/$releasever/7/ge
:wq 

yum clean all
yum makecache


注意，一定要选择匹配对应系统的文件才能安装成功。